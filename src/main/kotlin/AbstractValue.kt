/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 23/04/18.
 */

sealed class AbstractValue {

    abstract infix fun C(it: AbstractValue): Boolean

    infix fun C(other: Int) = C(Constant(other))

    abstract operator fun plus(other: AbstractValue): AbstractValue

    // intersection
    open infix fun n(other: AbstractValue): AbstractValue = Intersection(this, other)

    infix fun n(other: Int) = n(Constant(other))

    // union
    open infix fun U(other: AbstractValue): AbstractValue = Union(this, other)

    infix fun U(other: Int) = U(Constant(other))

    operator fun not() = Not(this)

    override fun equals(other: Any?): Boolean {
        return other is AbstractValue && other.C(this) && this.C(other)
    }

    operator fun unaryMinus() = negate()

    abstract fun negate(): AbstractValue
}

fun Intersection(v1: AbstractValue, v2: AbstractValue) = !(Union(!(v1), !(v2)))

class Union(val v1: AbstractValue, val v2: AbstractValue) : AbstractValue() {
    override fun C(it: AbstractValue): Boolean {
        return v1.C(it) || v2.C(it)
    }

    override fun plus(other: AbstractValue): AbstractValue {
        return Union(v1.plus(other), v2.plus(other))
    }

    override fun negate() = -v1 U -v2
}

class Not(val v1: AbstractValue) : AbstractValue() {

    override fun C(it: AbstractValue): Boolean {
        return when (it) {
            is Not -> v1.C(it.v1)
            else -> it.C(this)
        }
    }

    override fun plus(other: AbstractValue): AbstractValue =
            if (other is Not) !(v1 + other.v1)
            else other.plus(this)

    override fun negate() = Not(-v1)
}

object Variable : AbstractValue() {
    override fun C(it: AbstractValue): Boolean = true
    override fun plus(other: AbstractValue): AbstractValue = this
    override fun negate(): AbstractValue = this
}

fun LessThan(bound: AbstractValue) = Not(Union(MoreThan(bound), bound))

fun LessThan(bound: Int) = LessThan(Constant(bound))

fun Range(v1: AbstractValue, v2: AbstractValue) = MoreThan(v1) U LessThan(v2)

class MoreThan(bound: AbstractValue) : AbstractValue() {

    constructor(bound: Int) : this(Constant(bound))

    val bound: AbstractValue

    init {
        if (bound is MoreThan) this.bound = bound.bound
        else this.bound = bound
    }

    override fun plus(other: AbstractValue): AbstractValue {
        return MoreThan(bound.plus(other))
    }

    override fun C(it: AbstractValue): Boolean {
        return when (bound) {
            is Constant -> when (it) {
                is Constant -> {
                    return it.actualValue > bound.actualValue
                }
                is Union -> C(it.v1) || C(it.v2)
                is Not -> {
                    when (it.v1) {
                        is Not -> C(it.v1.v1)
                        is Union -> !C(it.v1.v1)&& !C(it.v1.v2)
                        Variable -> false
                        is MoreThan -> C(it.v1.bound)
                        is Constant -> true
                    }
                }
                is Variable -> true
                is MoreThan -> C(it.bound) || bound C it
            }
            is Union -> bound.v1.C(it) || bound.v1.C(it)
            is Not -> !bound.C(it)
            is Variable -> true
            is MoreThan -> bound.C(it)
        }
    }

    override fun negate(): AbstractValue = LessThan(-bound)
}

class Constant(val actualValue: Int) : AbstractValue() {

    override fun C(it: AbstractValue): Boolean =
            when (it) {
                is Constant -> it.actualValue == actualValue
                is MoreThan -> {
                    when (it.bound) {
                        is Constant -> this.actualValue > it.bound.actualValue
                        else -> it.C(this)
                    }
                }
                is Not -> !C(it.v1)
                else -> it.C(this)
            }

    override fun plus(other: AbstractValue): AbstractValue =
            when (other) {
                is Constant -> Constant(this.actualValue + other.actualValue)
                is Union -> Union(other.v1.plus(this), other.v2.plus(this))
                is Not -> Not(other.v1 + this)
                is Variable -> other
                is MoreThan -> other.plus(this)
            }

    override fun negate(): AbstractValue = Constant(-actualValue)
}

class Reference(val length: AbstractValue, private val values: kotlin.Array<AbstractValue>) {

    fun C(it: AbstractValue): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    operator fun set(index: AbstractValue, value: AbstractValue) {
        if (index is Constant) {
            values[index.actualValue] = value
        } else {
            for (ind in values.indices.filter { index.C(Constant(it)) }) {
                if (index.C(Constant(ind))) {
                    values[ind] = Union(value, values[ind])
                }
            }
        }
    }

    operator fun get(index: AbstractValue): AbstractValue {
        return if (index is Constant) values[index.actualValue]
        //if the index is unknown, the variable may have the value of any of the variables in this array
        else {
            val findIndex = values.withIndex().find { index.C(Constant(it.index)) }
                    ?: throw Exception("Illegal index $index")
            var value = findIndex.value
            for (i in (findIndex.index + 1)..values.size) {
                if (index.C(Constant(i))) {
                    value = Union(value, values[i])
                }
            }
            return value
        }
    }

    val isConstant = !values.any { it !is Constant }

    fun plus(other: AbstractValue): AbstractValue {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}