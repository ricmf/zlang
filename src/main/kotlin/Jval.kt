import jasm.attributes.Code
import jasm.io.ClassFileReader
import jasm.lang.Bytecode
import jasm.lang.ClassFile
import jasm.lang.JvmType
import java.util.*

/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 14/04/18.
 */
//
//val funcionOverrides: Map<ClassFile.Method, (Bytecode) -> Bytecode> = mapOf()
//
//fun evaluatePartially(clazz: ClassFile): ClassFile {
//    clazz.methods().forEach(::evaluatePartially)
//    return clazz
//}
//
////val pureMethods = hashMapOf<ClassFile.Method, Boolean>()

//fun evaluatePartially(method: ClassFile.Method) {
//    val (index, code) = findCode(method) ?: throw Exception("Method has no code")
//    val impure = code.bytecodes().any(::isImpure)
//    pureMethods[method] = impure
//    method.attributes()[index] = Code(evaluatePartially(code.bytecodes()), code.handlers(), method)
//}



fun <T> List<T>.secondLast() = this[this.size - 2]

fun evaluatePartially(bytecodes: List<Bytecode>): List<Bytecode> {
    val inlinedCode = inlineAll(bytecodes)
    val abstractStack = Stack<AbstractValue>()
    for (code in inlinedCode) {
        when (code) {
            is Bytecode.ArrayStore -> {
                val value = abstractStack.pop() as AbstractValue
                val index = abstractStack.pop() as AbstractValue
                (abstractStack.pop() as Reference)[index] = value
            }
            is Bytecode.ArrayLength -> {
                abstractStack.add((abstractStack.pop() as Reference).length)
            }
            is Bytecode.ArrayLoad -> {
                val index = abstractStack.pop() as AbstractValue
                val array = abstractStack.pop() as Reference
                abstractStack.add(array[index])
            }
            is Bytecode.BinOp -> {
                val int1 = abstractStack.pop() as AbstractValue
                val int2 = abstractStack.pop() as AbstractValue
                when (code.op) {
                    Bytecode.BinOp.ADD -> {
//                        abstractStack.add(Variable<Int> { it. })
                    }
                    Bytecode.BinOp.SUB -> 1
                    Bytecode.BinOp.MUL -> 2
                    Bytecode.BinOp.DIV -> 3
                    Bytecode.BinOp.REM -> 4
                    Bytecode.BinOp.SHL -> 5
                    Bytecode.BinOp.SHR -> 6
                    Bytecode.BinOp.USHR -> 7
                    Bytecode.BinOp.AND -> 8
                    Bytecode.BinOp.OR -> 9
                    Bytecode.BinOp.XOR -> 10
                    else -> throw Exception("invalid code ${code.op}")
                }
            }
            is Bytecode.Branch -> 1
            is Bytecode.CheckCast -> 1
            is Bytecode.Cmp -> 1
            is Bytecode.Conversion -> 1
            is Bytecode.GetField -> 1
            is Bytecode.Goto -> 1
            is Bytecode.Dup -> {
                abstractStack.push(abstractStack.peek())
            }
            is Bytecode.DupX1 -> 2
            is Bytecode.DupX2 -> 2
            is Bytecode.Invoke -> 2
            else -> 2
        }
    }
    return bytecodes
}

fun findCode(method: ClassFile.Method) = method.attributes().withIndex().find { it is Code } as IndexedValue<Code>?

val methodBodyCache = mutableMapOf<JvmType.Function, List<Bytecode>>()

fun getBytecode(methodCall: Bytecode.Invoke): List<Bytecode> =
        if (methodCall.mode == Bytecode.InvokeMode.STATIC || methodCall.mode == Bytecode.InvokeMode.VIRTUAL) {
            methodBodyCache[methodCall.type]
                    ?: try {
                        val reader = ClassFileReader(AbstractValue::class.java.classLoader.getResourceAsStream((methodCall.owner as JvmType.Clazz).toString().replace(".", "/") + ".class"))
                        val cf = reader.readClass()
                        val methodDeclaration = cf.methods().find { it.type() == methodCall.type }
                                ?: throw Exception("Couldn't find method ${methodCall.type} on class $cf")
                        val code = methodDeclaration.attributes().first() as Code
                        code.bytecodes()
                    } catch (e: Exception) {
                        throw Exception("failed to read class ${methodCall.owner}", e)
                    }
        } else listOf(methodCall)

fun inlineAll(bytecodes: List<Bytecode>): List<Bytecode> = bytecodes.flatMap { code ->
    if (code is Bytecode.Invoke) {
        inlineAll(getBytecode(code))
    } else listOf(code)
}