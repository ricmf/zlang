/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 26/04/18.
 */


sealed class AbstractBit {

    open fun simplify() {

    }

    infix fun xor(other: AbstractBit): AbstractBit {
        return when {
            this == other -> Zero
            this == Zero && other == One -> One
            this == One && other == Zero -> One
            else -> Xor(this, other)
        }
    }

    infix fun and(other: AbstractBit): AbstractBit {
        return when {
            this == Zero -> Zero
            other == Zero -> Zero
            this == One && other == One -> One
            else -> And(this, other)
        }
    }

    operator fun not() = this xor One

    infix fun or(other: AbstractBit) = (this xor other) xor (this and other)

    override fun toString(): String {
        return when (this) {
            One -> "1"
            Zero -> "0"
            is Xor -> "(${this.bit1}⊕${this.bit2})"
            is And -> "(${this.bit1}&${this.bit2})"
            is Variable -> "U"
        }
    }

}

sealed class BinaryOp(var bit1: AbstractBit, var bit2: AbstractBit) : AbstractBit() {

    override fun simplify() {
        //comutativity
        val temp = bit1
        bit1 = bit2
        bit2 = temp
        bit1.simplify()
        bit2.simplify()

        // associativity
        if (bit1.javaClass == this.javaClass) {
            val temp = (bit1 as BinaryOp).bit2
            (bit1 as BinaryOp).bit2 = bit2
            bit2 = temp
            bit1 = (bit1 as BinaryOp).bit1 xor (bit1 as BinaryOp).bit2
            bit1.simplify()
        } else if (bit1 is BinaryOp) {
            bit1 = (bit1 as BinaryOp).bit1 and (bit1 as BinaryOp).bit2
        }
        if (bit2.javaClass == this.javaClass) {
            val temp = (bit2 as BinaryOp).bit1
            (bit2 as BinaryOp).bit1 = bit1
            bit1 = temp
            bit2 = (bit2 as BinaryOp).bit1 xor (bit2 as BinaryOp).bit2
            bit2.simplify()
        } else if (bit2 is BinaryOp) {
            bit2 = (bit2 as BinaryOp).bit1 and (bit2 as BinaryOp).bit2
        }
    }

}

object Zero : AbstractBit()
object One : AbstractBit()
class Variable : AbstractBit()

class Xor(bit1: AbstractBit, bit2: AbstractBit) : BinaryOp(bit1, bit2) {

    override fun equals(other: Any?): Boolean {
        return (bit1 xor bit2) === other
    }

}

class And(bit1: AbstractBit, bit2: AbstractBit) : BinaryOp(bit1, bit2)

operator fun Array<out AbstractBit>.plus(other: Array<AbstractBit>): Array<AbstractBit> {
    if (this.size != other.size) throw Exception("Different size arrays")
    return sumOrSub(other, Zero)
}

private fun Array<out AbstractBit>.sumOrSub(other: Array<AbstractBit>, carry: AbstractBit): Array<AbstractBit> {
    var carry1 = carry
    val newArr = Array<AbstractBit>(other.size) { Zero }
    for (i in this.indices) {
        val other = other[i] xor carry
        newArr[i] = this[i] xor other xor carry1
        carry1 = (this[i] and other) or (this[i] and carry1) or (other and carry1)
    }
    return newArr
}

operator fun Array<out AbstractBit>.minus(other: Array<AbstractBit>): Array<AbstractBit> {
    return sumOrSub(other, One)
}

fun Array<out AbstractBit>.binary(): String {
    return this.reversed().joinToString("")
}


fun Array<out AbstractBit>.mayEqualZero(): Boolean {
    return !this.any { it == One }
}

fun Array<out AbstractBit>.maybeMoreThanZero(): Boolean {
    return this[this.size - 1] != One
}


fun Array<out AbstractBit>.maybeLessThanZero(): Boolean {
    return this[this.size - 1] != Zero
}

fun Array<out AbstractBit>.simplify() = forEach { it.simplify() }

val String.binary
    get() = parse(this)

fun parse(s: String): Array<AbstractBit> {
    val arr = Array<AbstractBit>(s.length) { Zero }
    var i = arr.size
    for (c in s) {
        i -= 1
        arr[i] = if (c == '0') Zero else if (c == '1') One else Variable()
    }
    return arr
}

