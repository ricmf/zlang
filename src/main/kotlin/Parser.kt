import kotlin.reflect.KClass

/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 23/04/18.
 */

val keywords = setOf("function", "if", "else", "return", "var", "compile")


class AST {

}

class Expected(val index: Int, val s: String) : Exception("Expected $s at position $index")

fun expect(expected: String, actual: String, index: Int): Int {
    if (matches(actual, index, expected)) throw Expected(index, expected)
    return index + expected.length
}

private fun matches(actual: String, index: Int, expected: String) =
        actual.substring(index, expected.length) != expected

interface Parser {
    fun parse(code: String, index: Int, context: List<SyntaxElement.Statement.Declaration>): SyntaxElement
}

sealed class SyntaxElement() {

    companion object : Parser {
        override fun parse(code: String, index: Int, context: List<Statement.Declaration>): SyntaxElement {


        }
    }

    class Function(val parameters: List<Statement.Declaration>,
                   val statements: List<Statement>) : SyntaxElement() {

        companion object : Parser {
            override fun parse(code: String, index: Int, context: List<Statement1.Declaration>): SyntaxElement {
                var realIndex = index
                fun move() {
                    while (code[realIndex] == ' ' || code[realIndex] == '\n') realIndex++
                }

                fun expect(s: String) {
                    realIndex = expect(s, code, index)
                }
                move()
                expect("function")
                move()
                expect("(")
                move()
                realIndex++
                val parameters = if (code[realIndex] == ')') listOf<Statement.Declaration>()
                else {
                    val parameters = ArrayList<Statement.Declaration>()
                    while (code[realIndex] == ',') {
                        move()
                        parameters.add(Statement.Declaration.ParameterDeclarationParser.parse(code, realIndex, context.plus(parameters)) as Statement.Declaration)
                        move()
                    }
                    expect(")")
                    parameters
                }
                move()
                expect("{")
                move()
                val body = SyntaxElement.parse(code, index, context)
                return Function(listOf(), body)

            }
        }

    }

    abstract class Statement() : SyntaxElement() {


        class Declaration(val name: String) : Statement() {

            object LocalDeclarationParser : Parser {
                override fun parse(code: String, index: Int, context: List<Statement.Declaration>): SyntaxElement {
                    var index = index
                    fun expect(s: String) {
                        index = expect(s, code, index)
                    }

                    fun move() {
                        while (code[index] == ' ') index++
                    }
                    move()
                    expect("var")
                    move()
                    val name = StringBuilder()
                    while (code[index] != ' ') {
                        name.append(code[index])
                        index++
                    }
                    move()
                    expect("\n")
                    if (context.any { it.name == name.toString() }) throw Exception("Duplicate name $name")
                    return Declaration(name.toString())
                }
            }

            object ParameterDeclarationParser : Parser {
                override fun parse(code: String, index: Int, context: List<Statement.Declaration>): SyntaxElement {
                    var index = index
                    fun expect(s: String) {
                        index = expect(s, code, index)
                    }

                    fun move() {
                        while (code[index] == ' ') index++
                    }
                    move()
                    expect("var")
                    move()
                    val name = StringBuilder()
                    while (code[index] != ' ') {
                        name.append(code[index])
                        index++
                    }
                    move()
                    if (context.any { it.name == name.toString() }) throw Exception("Duplicate name $name")
                    return Declaration(name.toString())
                }
            }


        }


        class ArithmeticOperation : Statement() {

        }

        class Return(val variable: Declaration) : Statement() {

        }

        class If(val then: Statement, val els: Statement?) {

        }

        class While()
        class FunctionCall(val variables: List<Variable>)
    }


}


fun parse(code: String): AST {
    val tree: AST
    var context: Pair<Int, SyntaxElement>
    context = expect(code, context.first, context.second)
}
