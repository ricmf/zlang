/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 20/04/18.
 */


class TestClass {
    fun test() = listOf(1, 2)
    fun testMapz() = listOf(1, 2).map { it * 2 }
}