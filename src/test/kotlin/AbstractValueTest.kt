import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef></ricardomef>@gmail.com>, 21/04/18.
 */
class AbstractValueTest {

    @Test
    fun constantsTest() {
        val const1 = Constant(1)
        val const2 = Constant(2)
        assertFalse(const1 C const2)
        assertTrue(const1 C const1)
        assertTrue((const1 + const2) C 3)
    }

    @Test
    fun lowerBoundTest() {
        val positive = MoreThan(0)
        assertTrue(positive C Constant(5))
        assertFalse(positive C Constant(-1))
        assertTrue(MoreThan(-1) C MoreThan(1))

    }

    @Test
    fun unionTest() {
        val positive = MoreThan(0)
        val constant = Constant(-5)
        val disjunction = positive U constant
        assertTrue(disjunction C 5)
        assertFalse(disjunction C -1)
        assertTrue(disjunction C -5)
        val sum = disjunction + disjunction
        assertTrue(sum C -10)
        assertFalse(sum C -11)
        assertTrue(sum C -4)
        assertTrue(sum C MoreThan(-5))
        assertTrue(sum C positive)
        assertTrue(sum C -10)
    }

    @Test
    fun intersectionTest() {
        val positive = MoreThan(Constant(0))
        val constant = Constant(5)
        val intersection = positive n constant
        assertTrue(intersection C 5)
        assertFalse(intersection C -1)
        assertFalse(intersection C -5)
        assertTrue(intersection.equals(Constant(5)))
        val sum = intersection + intersection
        assertTrue(sum C 10)
        assertFalse(sum C -1)
        assertFalse(sum C 5)

    }

    @Test
    fun variableTest() {
        val a = Variable
        val b = Variable
        val sum = a + b

        val c = Variable
        assertTrue(sum.equals(c))
        val lower = MoreThan(c)
        assertTrue(sum.equals(lower))

        val sum2 = a n Constant(2)

//        assertFalse(sum.equals(sum2))


    }


    @Test
    fun complementTest() {
        val bound = MoreThan(3)
        (!bound).let { upperBound ->
            assertTrue(upperBound C 0)
            assertTrue(upperBound C 3)
            assertFalse(upperBound C 4)
        }
        LessThan(3).let { upperBound ->
            assertTrue(upperBound C 0)
            assertFalse(upperBound C 3)
            assertFalse(upperBound C 4)
        }
    }


    @Test
    fun negationTest() {
        assertTrue(-Constant(1) C Constant(-1))
        assertFalse(LessThan(-1) C MoreThan(1))
        assertFalse(-MoreThan(1) C MoreThan(1))
        assertFalse(-LessThan(1) C LessThan(1))

    }

    @Test
    fun negationTest2() {
        assertTrue(LessThan(-3) C MoreThan(-4))
    }


}