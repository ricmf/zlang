import org.junit.Assert.*
import org.junit.Test

/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 26/04/18.
 */


class BitTest {

    @Test
    fun notTest() {
        assertEquals(One, !Zero)
        assertEquals(Zero, !One)
    }

    @Test
    fun orTest() {
        assertEquals(Zero, Zero or Zero)
        assertEquals(One, Zero or One)
        assertEquals(One, One or Zero)
        assertEquals(One, One or One)
    }

    @Test
    fun sumTest() {
        //big endian
        val one = arrayOf(One, Zero)
        val two = arrayOf(Zero, One)
        assertEquals(two, one + one)
        assertEquals(two + one, one + one + one)
        assertEquals(arrayOf(Zero, Zero), two + two)
    }

    @Test
    fun subTest() {
        //big endian
        val zero = arrayOf(Zero, Zero)
        val one = arrayOf(One, Zero)
        val two = arrayOf(Zero, One)
        assertEquals(zero, one - one)
        assertEquals(one, two - one)
        assertEquals(arrayOf(One, One), zero - one)
    }


    @Test
    fun largeSum() {
        var int = Array<AbstractBit>(32) { Zero }
        val int2 = Array<AbstractBit>(32) { Zero }
        int2[0] = Variable()
        for (i in 1..1_00) {
            println(int.binary())
            int += int2
        }
        println(int.binary())
    }

    @Test
    fun parseTest() {
        assertEquals(arrayOf(Zero, One), parse("10"))
    }

    @Test
    fun abstractSumTest() {
        val one = "0001".binary
        val und = "000U".binary
        val sum = one + und
        assertFalse(sum.maybeLessThanZero())
        val sub = (sum - und)
        println(sub[0])
        fun simplify() {
            sub.simplify()
            println(sub[0])
        }
        for (i in 1..10) simplify()
        assertTrue(one[0] == sub[0])
        assertEquals(one, sub)
    }

}
