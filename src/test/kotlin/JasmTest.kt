import jasm.attributes.Code
import jasm.io.ClassFileReader
import jasm.lang.Bytecode
import org.junit.Test

/** Copyright (C) Ricardo Meneghin Filho - All Rights Reserved
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * Proprietary and confidential
 * Written by Ricardo Meneghin Filho <ricardomef@gmail.com>, 14/04/18.
 */


class JasmTest {

    @Test
    fun testa() {
        val fin = javaClass.classLoader.getResourceAsStream("TestClass.class")

        val reader = ClassFileReader(fin)
        val cf = reader.readClass()


        System.out.println("Read class called: " + cf.name())
        cf.methods().toList().map {
            val code = it.attributes()[0]
            (code as Code)
            code.bytecodes().map {
                if (it is Bytecode.Invoke) {
                    println(getBytecode(it))
                }
            }

        }
    }

}
